#!/bin/bash

if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

CONFIG_FILE="install.properties"
CONFIG_PATH="/etc/influxdb/"
CONFIG_FILES=(influxdb.conf)
INFLUXDB_PORT="8086"
INFLUXDB_DATABASE_FOLDER="/var/lib/influxdb"

SERVICE_PATH="/lib/systemd/system"
INFLUXDB_SERVICE="influxdb.service"
SERVICES=($INFLUXDB_SERVICE)

DownloadAndExtract() 
{
   sudo apt-get -f install;
   curl -sL https://repos.influxdata.com/influxdb.key | sudo apt-key add -;
   source /etc/lsb-release;
   echo "deb https://repos.influxdata.com/${DISTRIB_ID,,} ${DISTRIB_CODENAME} stable" | sudo tee /etc/apt/sources.list.d/influxdb.list;
   sudo apt-get update && sudo apt-get install influxdb;
   CopyConfiguration
}

CopyConfiguration()
{
   echo "--> Copying configuration."
   if [ ! -d ${CONFIG_PATH} ] ; then
      mkdir ${CONFIG_PATH}
   else
      rm -f ${CONFIG_PATH}/*
   fi
   #replace all the variables in configuration files
   for prop in ${CONFIG_FILES[@]}; 
   do
     sudo cp ./$prop ${CONFIG_PATH}
     ReplaceVariables ${CONFIG_PATH}/$prop
   done
}

ReplaceVariables()
{
   sed -i -e "s,@INFLUXDB_PORT@,"${INFLUXDB_PORT}",g" $1
   sed -i -e "s,@INFLUXDB_DATABASE_FOLDER@,"${INFLUXDB_DATABASE_FOLDER}",g" $1
}

UninstallAndDelete()
{
	sudo apt-get remove influxdb;
}

RegisterServices()
{  
   for prop in ${SERVICES[@]}; 
   do
     RegisterService $prop
   done
}

RegisterService()
{
	echo "registering " 
	echo $SERVICE_PATH/$1
   systemctl enable $SERVICE_PATH/$1
}

UnRegisterServices()
{
   for prop in ${SERVICES[@]}; 
   do
     UnRegisterService $prop
   done
}

UnRegisterService()
{
   systemctl disable $1
}

StartServices()
{
   for prop in ${SERVICES[@]}; 
   do
     StartService $prop
   done
}

StartService()
{
   systemctl start $1
}

StopServices()
{
   for prop in ${SERVICES[@]}; 
   do
     StopService $prop
   done
}

StopService()
{
   systemctl stop $1
}

StatusServices()
{
   for prop in ${SERVICES[@]}; 
   do
     StatusService $prop
   done
}

StatusService()
{
   systemctl status $1
}


ReadConfigurationFromFile()
{
   IFS="="
   while read -r name value
   do
   case "$name" in
   "influxdb.port")
         INFLUXDB_PORT=$value
         ;;
   "influxdb.databasefolder")
         INFLUXDB_DATABASE_FOLDER=$value
         ;;
         
   esac
   done < $1
}

CheckRequiredParameters()
{
   if [ "$INFLUXDB_PORT" == "" ]
   then
      echo "Enter port of influxdb"
      read INFLUXDB_PORT
   fi
   if [ "$INFLUXDB_DATABASE_FOLDER" == "" ]
   then
      echo "Enter influx database folder location"
      read $INFLUXDB_DATABASE_FOLDER
   fi
}

LoadSettings()
{
   if [ -f "$CONFIG_FILE" ]
   then
      echo "== Read settings from file =="
      ReadConfigurationFromFile $CONFIG_FILE
   fi
   CheckRequiredParameters
}

ShowHelp() 
{
   echo " -h			Show this help"
   echo " -i			Download and install all the files and services and start them"
   echo " start			Start the services"
   echo " stop			Stop the services"
   echo " register		Register the services"
   echo " deregister	Deregister the services"
   echo " status		Status the services"
   echo " -u			Uninstall all the services and delete files"
}

case "$1" in
   -u)   echo "!! ->    Uninstalling ...."  
         StopServices
         sleep 1 
         UnRegisterServices
         UninstallAndDelete
         ;;
   -i)   echo  "!! ->  Installing ...."
         LoadSettings
         DownloadAndExtract
         RegisterServices
         ;;
   register)
         echo  "!! ->  Registering services ...."
         RegisterServices
         ;;
   deregister)
         echo  "!! ->  Unregistering services ...."
         UnRegisterServices
         ;;
   start)
         echo  "!! ->  Starting services ...."
         CopyConfiguration
         StartServices
         LoadSettings
         ;;
   stop) echo  "!! ->  Stopping services ...."
         StopServices         
         ;;
   status)
         StatusServices         
         ;;
   log)
         for prop in ${SERVICES[@]}; 
         do
            journalctl -u $prop > /media/sf_shared/$prop.log
         done
         
         ;;
   *)    ShowHelp
         ;;
esac